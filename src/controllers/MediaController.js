const { response } = require('express');
const User = require('../models/User');
const Comment = require('../models/Comment');
const Media = require('../models/Media');


const index = async(req,res) => {
    try {
        const medias = await Media.findAll();
        return res.status(200).json({medias});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const media = await Media.findByPk(id);
        return res.status(200).json({media});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
          const media = await Media.create(req.body);
          return res.status(201).json({message: "Mídia criada com sucesso!", media: media});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Media.update(req.body, {where: {id: id}});
        if(updated) {
            const media = await Media.findByPk(id);
            return res.status(200).send(media);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Mídia não encontrada");
    }
};
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Media.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Mídia deletada com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Mídia não encontrada.");
    }
};

const addRelationComment = async(req,res) => {
    const {id} = req.params;
    try {
        const media = await Media.findByPk(id);
        const comment = await Comment.findByPk(req.body.CommentId);
        await media.addComment(comment);
        return res.status(200).json(media);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelationComment = async(req,res) => {
    const {id} = req.params;
    try {
        const media = await Media.findByPk(id);
        await media.setComment(null);
        return res.status(200).json(media);
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationComment,
    removeRelationComment
};
