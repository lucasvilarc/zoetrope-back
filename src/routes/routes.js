const { Router } = require('express');
const UserController = require('../controllers/UserController');
const CommentController = require('../controllers/CommentController');
const MediaController = require('../controllers/MediaController');


const router = Router();

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users',UserController.create);
router.get('/users/:id',UserController.show);
router.put('/users/:id', UserController.update);
router.put('/usersaddrole/:id', UserController.addRelationComment);
router.delete('/usersremoverole/:id', UserController.removeRelationComment);
router.delete('/users/:id', UserController.destroy);


router.get('/comments',CommentController.index);
router.get('/comments/:id',CommentController.show);
router.post('/comments',CommentController.create);
router.get('/comments/:id',CommentController.show);
router.put('/comments/:id', CommentController.update);
router.put('/commentsaddrole/:id', CommentController.addRelationComment);
router.delete('/commentsremoverole/:id', CommentController.removeRelationComment);
router.delete('/comments/:id', CommentController.destroy);


router.get('/medias',MediaController.index);
router.get('/medias/:id',MediaController.show);
router.post('/medias',MediaController.create);
router.get('/medias/:id',MediaController.show);
router.put('/medias/:id', MediaController.update);
router.put('/mediasaddrole/:id', MediaController.addRelationComment);
router.delete('/mediasremoverole/:id', MediaController.removeRelationComment);
router.delete('/medias/:id', MediaController.destroy);

module.exports = router;
