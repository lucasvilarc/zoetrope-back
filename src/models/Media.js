const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Media = sequelize.define('Media', {   
    genre: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    year: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    rating: {
        type: DataTypes.FLOAT
    },

    duration: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    // timestamps: false
});

Media.associate = function(models) {
    Media.hasMany(models.Comment, { });
}

module.exports = Media;