const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Comment = sequelize.define('Comment', {  
    date: {
        type: DataTypes.DATE,
        allowNull: false
    },

    content: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    // timestamps: false
});

Comment.associate = function(models) {
    Comment.belongsTo(models.User, { });
    Comment.belongsTo(models.Media, { });

}

module.exports = Comment;